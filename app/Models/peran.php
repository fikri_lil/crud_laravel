<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class peran extends Model
{
    use HasFactory;
    public function film()
  {
    return $this->belongsTo(film::class, 'film_id');
  }
  
  public function cast()
  {
    return $this->belongsTo(cast::class, 'cast_id');
  }
}
