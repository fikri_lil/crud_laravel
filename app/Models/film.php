<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    use HasFactory;
    public function genre()
  {
    return $this->belongsTo(genre::class);
  }

  public function film(){
    return $this->hasOne(film::class, 'film_id');
}
  
}
