<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\profile;
use Illuminate\Support\Facades\Auth;
class ProfileController extends Controller
{
    public function index(){
        $iduser = Auth::id();

        $detailProfile = profile::where('user_id', $iduser)->first();

        return view('profile.index', ['detailProfile' => $detailProfile]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'umur'=> 'required',
            'alamat'=> 'required',
            'bio'=> 'required',
        ]);
        $profile = profile::find(1);
 
        $profile->umur = $request -> umur;
        $profile->alamat = $request -> alamat;
        $profile->bio = $request -> bio;
 
        $profile->save();
        
        return redirect('/profile');
    }
}
