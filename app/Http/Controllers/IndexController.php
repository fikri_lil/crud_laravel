<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        return view('halaman.index');
    }
    
    public function kirim(Request $request){
        $nama = $request['name'];

        return view('welcome', ['nama'=> $nama]);
    }

}
