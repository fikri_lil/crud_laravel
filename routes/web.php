<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\ProfileController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'index']);

Route::get('/register', [AuthController::class, 'register']);

Route::post('/welcome', [IndexController::class, 'kirim']);

Route::get('/data-table',function(){
    return view('halaman.table');
});

Route::middleware(['auth'])->group(function () {

//CRUD Cast
// Create
 //route menuju form create
 Route::resource('cast',CastController::class);
 
 //Profile
 Route::resource('profile',ProfileController::class)->only(['index', 'update']);
});

Auth::routes();
