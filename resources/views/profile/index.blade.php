@extends('layout.master')

@section('judul')
Update Profile
@endsection

@section('content')

<form action = '/profile/{{$detailProfile->id}}' method='POST'>
    @csrf
    @method('put')
    <div class="mb-3">
      <label class="form-label">Nama User</label>
      <input type="text"  value="{{$detailProfile->user->name}}" class="form-control" disabled>
    </div>
    <div class="mb-3">
      <label class="form-label">Email</label>
      <input type="text"  value="{{$detailProfile->user->email}}" class="form-control" disabled>
    </div>
    <div class="mb-3">
      <label class="form-label">Umur</label>
      <input type="number" name="umur" value="{{$detailProfile->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }} </div>
    @enderror
    <div class="mb-3">
      <label >Alamat</label>
      <textarea name="alamat" class="form-control" cols="30" rows="10">{{$detailProfile->alamat}}</textarea>
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }} </div>
    @enderror
    <div class="mb-3">
      <label >Biodata</label>
      <textarea name="bio" class="form-control" cols="30" rows="10">{{$detailProfile->bio}}</textarea>
    </div>
    @error('bio')
      <div class="alert alert-danger">{{ $message }} </div>
    @enderror
    <button type="submit" class="btn btn-success">Submit</button>
  </form>

@endsection